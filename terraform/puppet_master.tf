provider "aws" {
  region = "us-east-1"
  access_key = "AKIAZUUTKG5EKNWYZOOZ"
  secret_key = "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"
}

# Use existing VPC
data "aws_vpc" "existing_vpc" {
  id = "vpc-00b285914e48b6032"
}

# Use existing subnet
data "aws_subnet" "existing_subnet" {
  vpc_id = data.aws_vpc.existing_vpc.id
  id     = "subnet-097e27c0efbe5e0a4"
}

# Use existing security group
data "aws_security_group" "existing_security_group" {
  id = "sg-0d848c7bf5c5b8b85"
}

resource "aws_instance" "puppetMaster" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.medium"
  subnet_id     = data.aws_subnet.existing_subnet.id
  key_name      = "myec2key"
  associate_public_ip_address = true
  security_groups = [data.aws_security_group.existing_security_group.id]   // Attach security group to instance

   connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("myec2key.pem")
    host        = aws_instance.puppetMaster.public_ip
   }
   provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver"
    ]
  }

  tags = {
    Name = "puppetMaster"
  }
}
