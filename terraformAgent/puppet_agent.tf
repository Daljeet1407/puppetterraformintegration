provider "aws" {
  region = "us-east-1"
  access_key = "AKIAZUUTKG5EKNWYZOOZ"
  secret_key = "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"
}


# Use existing VPC
data "aws_vpc" "existing_vpc" {
  id = "vpc-00b285914e48b6032"
}

# Use existing subnet
data "aws_subnet" "existing_subnet" {
  vpc_id = data.aws_vpc.existing_vpc.id
  id     = "subnet-097e27c0efbe5e0a4"
}

# Use existing security group
data "aws_security_group" "existing_security_group" {
  id = "sg-0d848c7bf5c5b8b85"
}

resource "aws_instance" "puppet-Agent" {
  ami           = "ami-07d9b9ddc6cd8dd30"

  instance_type = "t2.micro"
  key_name      = "myec2key"

  tags = {
    Name = "puppet-Agent"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("myec2key.pem")
    host        = aws_instance.puppet-Agent.public_ip
    timeout     = "5m" 
  }


  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.51.100 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"
    ]
  }

}

output "public_ip" {
  value = aws_instance.puppet-Agent.public_ip
}
